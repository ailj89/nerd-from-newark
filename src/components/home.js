import React from 'react';

const Home = () =>{
	return(
		<div>
			<h2 className='title is-spaced is-3'><span>Welcome</span></h2>
			
			<p className='subtitle is-5'>
			I am Andre Jordan, Front-End Developer, and here you will find details of the companies I worked for and projects I worked on. 
			</p>

			<p className='subtitle is-5'>
				Lover of all things geeky/nerdy, I stumbled upon HTML and CSS in 2006. 
				My career as a Web Developer began in 2008 and I also had a hand in education as a volunteer for Coder Dojo, 
				an Expert-in-Residence (teacher's assistant) for Web Development Immersive (WDI/Full-Stack) 
				and Front-End Web Development (FEWD) classes at General Assembly, 
				a mentor for Thinkful, and as an instructor at 3rdWard.
			</p>

			<p className="subtitle is-5">As of now, I am focusing on implementing <em>React</em> in my tech stack which is the point of this portfolio site. Which began as a static site, now includes <em>routing</em> and a <em>HTTP service</em> with a <em>promise</em> to a local json file setting the <em>state</em> of roles.</p>
		</div>
	)
}

export default Home;