import React from 'react';
import Role from './role';

const RoleList = (props) => {
	return(
		<div id='roles' className='roles'>
			<h2 className='title is-3'><span>Roles</span></h2>
			{props.roles.map(role => <Role key={role.company} {...role}/>)}
		</div>
	);		
}

export default RoleList;