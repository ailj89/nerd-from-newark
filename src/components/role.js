import React from 'react';

const Role = (props) =>{
	return(
		<div className='has-border-bottom'>
			<a href={props.url} target='_blank' title={props.company} rel="noopener noreferrer"><img width='75' src={props.company_logo} alt={props.company} /></a>
			<h3 className='title is-3'>{props.company}</h3>
			<h6 className='subtitle is-6'>{props.date_employed}</h6>
			{props.projects.map((project, i) => 
				<div className='project' key={i}>
				<h3 className='is-italic'>
					{function(){
						if (project.url) {
							return <a href={project.url} target='_blank' title={project.project_name} rel="noopener noreferrer">{project.project_name}</a>
						} else {
							return project.project_name
						}
					}.call(this)}
				</h3>
				<p>{project.project_details}</p>
				</div>
			)}
		</div>
	)
}

export default Role;