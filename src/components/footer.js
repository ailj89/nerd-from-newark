import React, { Component } from 'react';

class Footer extends Component{
	render(){
		return(
			<footer id='contact'>
				<div className='container is-clearfix'>
					<h2 className='title is-3'><span>Contact</span></h2>
					<ul>
						<li><a href='tel:+017812666602' rel='nofollow'>(781) 266-6602</a></li>
						<li><a href='mailto:alamontj89@gmail.com' rel='nofollow'>alamontj89@gmail.com</a></li>
						<li><a href='https://www.linkedin.com/in/andre-l-jordan/' target='_blank' rel='noopener noreferrer'>LinkedIn</a></li>
						<li><a href='../assets/Andre_Jordan_Front_End_Web_Developer.pdf' download='Andre_Jordan_Front_End_Web_Developer' target='_blank' rel='noopener noreferrer'>Resume</a></li>
					</ul>
						<p style={{marginTop: 10 + 'px'}}><em>Built with React</em></p>
				</div>
			</footer>
		)
	}
}

export default Footer;