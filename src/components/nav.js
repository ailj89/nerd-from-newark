import React from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../logo.png';

const Nav = () =>{
	return(
		<header className='App-header'>
			<div className='container'>
				<img src={logo} className='App-logo' alt='logo' />
				<h1 className='App-title title is-1'>Nerd From Newark</h1>
				<nav className='navbar'>
					<ul>
						<li>
							<NavLink exact={true} activeClassName="is-active" to="/">Home</NavLink>
						</li>
						<li>
							<NavLink activeClassName="is-active" to="/roles">Roles</NavLink>
						</li>
						<li>
							<NavLink activeClassName="is-active" to="/samples">Samples</NavLink>
						</li>
					</ul>
				</nav>
			</div>
		</header>
	)
}

export default Nav;