import React, { Component } from 'react';

class Samples extends Component{
	render(){
		return(
			<div>
				<h2 className='title is-3'><span>Code Samples</span></h2>

				<ul>
					<li><a href="https://codepen.io/ailj89/pen/rlJke" title="Canvas Practice" target="_blank" rel="noopener noreferrer">Canvas Practice</a></li>
					<li><a href="https://codepen.io/ailj89/pen/eZNGRY" title="Word Filter" target="_blank" rel="noopener noreferrer">Word Filter</a></li>
					<li><a href="https://bitbucket.org/ailj89/itunes-search/src/master/" title="iTunes Search" target="_blank" rel="noopener noreferrer">iTunes Search</a></li>
					<li><a href="https://bitbucket.org/ailj89/drag-and-drop/src/master/" title="Image Drag and Drop" target="_blank" rel="noopener noreferrer">Image Drag and Drop</a></li>
				</ul>
			</div>
		)
	}
}

export default Samples;