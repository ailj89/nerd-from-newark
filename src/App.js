import React, { Component } from 'react';
import '../node_modules/bulma/css/bulma.css';
import './App.css';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Nav from './components/nav';
import Home from './components/home';
import Samples from './components/samples';
import Footer from './components/footer';
import HttpService from './services/http-service';

class DynamicImport extends Component{
	state = {
		component: null
	}

	componentWillMount(){
		this.props.load()
		.then((mod) => this.setState(() => ({
			component: mod.default
		})))
	}
	render(){
		return this.props.children(this.state.component)
	}
}

const RoleList = (props) => (
	<DynamicImport load={() => import('./components/rolelist')}>
		{(Component) => Component === null
		? <h1>Loading Roles...please wait</h1>
		: <Component {...props} />}
	</DynamicImport>
)

const http = new HttpService();

class App extends Component{
	
	constructor(props){
		super(props);

		//es6 bind function | binding http service to display data from promise
		this.loadData = this.loadData.bind(this);

		this.loadData();
	}

	loadData = () =>{
		http.getRoles().then(roles =>{
			this.setState({roles: roles});
		}, err =>{
		});
	}

	state = {
		roles: []
	}

	render(){
		return(
			<Router>
				<div>
					<Nav />

					<Switch>
						<div className='container' style={{padding: 2 + 'rem'}}>
							
							<Route exact path='/' component={Home} />
							<Route path='/roles' render={(props) => <RoleList roles={this.state.roles} />} />
							<Route path='/samples' component={Samples} />
						</div>
					</Switch>

					<Footer />
				</div>
			</Router>
		)
	}
}

export default App;