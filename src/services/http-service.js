import 'whatwg-fetch';

class HttpService{
	getRoles = () =>{
		var promise = new Promise((resolve, reject) =>{
			fetch('../assets/roles.json')
			.then(response => {
					resolve(response.json());
			})
		})
		return promise;
	}
}

export default HttpService;