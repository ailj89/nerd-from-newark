/** @jsx React.DOM */

const Role = (props) =>{
	return(
		<div>
			<h2>{props.company}</h2>
			<img src={props.company_logo} />
			<span>{props.date_employed}</span>
			<p>{props.details}</p>
			<a href={props.url} target='_blank'>{props.company}</a>
		</div>
	);
}

const RoleList = (props) => {
	return(
		<div>
			{props.roles.map(role => <Role {...role} />)}
		</div>
	);
}

class App extends React.Component{
	state = {
		roles: [
			{	company: 'Inc Design',
				date_employed:'March 2018 - April 2018',
				details: 'Front-End Developer responsible for creating or updating and maintaining existing sites',
				company_logo: '../images/logos/incdesign.webp',
				url: 'https://www.incdesign.com/',
				projects:[
					{
						project_name:'W. R. Berkley 2017 Annual Report',
						project_details: 'Redesign of W. R Berkley\'s Annual Report site, including animations, updated with 2017 stats. Launched April 19, 2018.',
						project_image: '../images/screenshots/',
						url: 'https://products.wrberkley.com/AR/index.html'
					}					
				]
			},
			{	company: 'AllianceBernstein',
				date_employed:'September 2016 - January 2018',
				details: 'Front-End Developer responsible for creating or updating and maintaining existing sites',
				company_logo: '../images/logos/alliancebernstein.png',
				url: 'https://www.alliancebernstein.com/',
				projects: [
					{
						project_name:'Corporate Responsibility',
						project_details: 'Highlights AllianceBernstein\'s efforts in Global Sustainability, giving back to communities, and active participation in equality and representation.',
						project_image: '../images/screenshots/',
						url: 'https://www.alliancebernstein.com/corporate/our-firm/corporate-responsibility.htm'
					},
					{
						project_name:'Inside the Mind of Plan Sponsors',
						project_details: 'In 2016, AllianceBernstein defined contribution team conducted a web-based survey of over 1,000 plan sponsors. This site focuses on the trends and problem areas as reported by the sponsors.',
						project_image: '../images/screenshots/',
						url: 'https://www.alliancebernstein.com/investments/us/retirement/inside-the-minds-of-plan-sponsors.htm'
					},
					{
						project_name:'Institutions pages for The Americas, EMEA, and Asia-Pacific',
						project_details: 'Overview of investment solutions offered by AllianceBernstein for Alternatives, Equities, Fixed-Income, Multi-Asset and Defined Contribution.',
						project_image: '../images/screenshots/',
						url: 'https://www.alliancebernstein.com/institutions/americas/home.htm'
					}
				]
			},
			{
				company: 'Mammoth Advertising',
				date_employed:'January 2016 - March 2016',
				details: 'Front-End Developer responsible for creating sites for motion pictures.',
				company_logo: '../images/logos/',
				url: 'https://www.mammothnyc.com/',
				projects: [
					{
						project_name:'My Big Fat Greek Wedding 2',
						project_details: 'Site dedicated to the motion picture which displayed the trailer and gifs of scenes from the film.',
						project_image: '../images/screenshots/',
						url: 'https://web.archive.org/web/20160331120526/http://mybigfatgreekweddingmovie.com/'
					},
					{
						project_name:'Share A Praryer: Miracles From Heaven',
						project_details: 'An e-card generator, via Canvas, where users selected a message, a background, and entered a recipient. The generated e-card may be downloaded by users or shared via social media.',
						project_image: '../images/screenshots/',
						url: ''
					}
				]
			},
			{	company: 'Thinkful',
				date_employed:'2014 -  2014',
				details: 'Front-End Developer responsible for creating or updating and maintaining existing sites',
				company_logo: '../images/logos/',
				url: 'https://www..com/',
				projects: [
					{
						project_name:'Front-End Web Development Mentor',
						project_details: 'Held weekly one-on-one sessions with students to answer questions based on course material. Provided insightful feedback on assignments.',
						project_image: '../images/screenshots/',
						url: ''
					}
				]				
			},
			{	company: '3rd Ward',
				date_employed:'August 2014 - October 2014',
				details: 'Front-End Instructor',
				company_logo: '../images/logos/',
				url: 'https://www..com/',
				projects: [
					{
						project_name:'Front-End Web Developement Instructor',
						project_details: 'Taught a HTML5/CSS3 course and a jQuery course. Provided insightful feedback on assignments.',
						project_image: '../images/screenshots/',
						url: ''	
					}
				]				
			},
			{	company: 'General Assembly',
				date_employed:'September 2012 - July 2014',
				details: 'Front-End Developer responsible for creating or updating and maintaining existing sites',
				company_logo: '../images/logos/',
				url: 'https://www..com/',
				projects: [
					{
						project_name:'Front-End Web Developement (FEWD) Expert-in-Residence',
						project_details: 'Assisted in educating students who were interested in HTML, CSS, JavaScript, and jQuery. Held tutoring sessions on-site and online. Provided insightful feedback on assignments.',
						project_image: '../images/screenshots/',
						url: ''
					}
				]				
			}
		]
	}

	render(){
		return(
			<div>
				<h1>Nerd From Newark</h1>
				<RoleList roles={this.state.roles} />
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('app'));